
const weatherMiddleware = (service) => async (req, res, next) => {
  req.weatherData = await service.getWeather()
  return next()
}

module.exports = (service) => weatherMiddleware(service)