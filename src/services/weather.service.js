const moment = require('moment');
const puppeteer = require('puppeteer');

module.exports = class WeatherService {
  constructor(siteUrl) {
    this.weatherData = null
    this.siteUrl = siteUrl
    this.lastSync = null
  }

  async getWeather() {
    const syncPromise = this.syncWeatherIfNeeded();
    if (this.lastSync == null) {
      await syncPromise
      return this.weatherData
    }
    return this.weatherData
  }

  async syncWeatherIfNeeded() {
    if (this.lastSync == null || moment().diff(this.lastSync, 'seconds') > 30) {
      this.weatherData = await fetchWeather(this.siteUrl);
      this.lastSync = moment();
    }
  }
}

const fetchWeather = async (siteUrl) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(siteUrl);

  const textContent = await page.evaluate(() => {
    let childrenCount = document.querySelector('#wcard table tbody').children.length
    let mapTable = [
      {key: 'feelsLike', value: undefined},
      {key: 'rain', value: undefined},
      {key: 'wind', value: undefined},
      {key: 'uv', value: undefined},
      {key: 'sun', value: undefined},
    ]
    for (let i = 0; i < childrenCount; ++i) {
      mapTable[i].value = document.querySelector('#wcard table tbody').children[i].children[1].textContent
    }
    mapTable = mapTable.reduce((acc, cur) => {
      acc[cur.key] = cur.value
      return acc
    }, {})

    const currentWeather = {
      city: document.querySelector('#wcard h6').childNodes[2].textContent,
      temperature: {
        actual: parseFloat(document.querySelector('#wcard h1').textContent.match(/(\d+([.,]\d+)?)/)[0].replace(',', '.')),
        feelsLike: parseFloat(mapTable.feelsLike.match(/(\d+([.,]\d+)?)/)[0].replace(',', '.')),
      },
      rain: parseFloat(mapTable.rain.match(/(\d+([.,]\d+)?)/)[0].replace(',', '.')),
      wind: parseFloat(mapTable.wind.match(/(\d+([.,]\d+)?)/)[0].replace(',', '.')),
      uv: parseFloat(mapTable.uv.match(/(\d+([.,]\d+)?)/)[0].replace(',', '.')),
      sunrise: mapTable.sun.split('/')[0],
      sunset: mapTable.sun.split('/')[1],
    }

    return currentWeather
  });

  const weather = {
    ...textContent,
    sunrise: moment(textContent.sunrise, 'HH:mm').toDate(),
    sunset: moment(textContent.sunset, 'HH:mm').toDate(),
  }

  browser.close();

  return weather
}