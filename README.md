# Weather API

### Getting started

The app was tested on `Node v10.16.1`

##### Install

For `npm` users run

```
$ npm install
```

For `yarn` users run

```
$ yarn install
```

##### Running server

For `npm` users run

```
// start a dev server (nodemon)
$ npm run dev 

// start a normal server
$ npm start
```

For `yarn` users run

```
// start a dev server (nodemon)
$ yarn dev 

// start a normal server
$ yarn start
```


### Using the API

Start the server and perform a `GET` request on `http://localhost:3000/`

> Please note: first request might take a bit longer. After that the server will lazy scrape dmi.dk for fresh data.

the response should look something like this

```javascript
{
	"success": true,
	"data": {
		"city": "København",
        "temperature": {
            "actual": 7,
            "feelsLike": 5
        },
        "rain": 0,
        "wind": 4,
        "uv": 0.3,
        "sunrise": "2020-01-16T07:28:00.000Z",
        "sunset": "2020-01-16T15:11:00.000Z"
    }
}
```