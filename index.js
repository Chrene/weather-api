const express = require('express');
const expressRateLimit = require('express-rate-limit')
const WeatherService = require('./src/services/weather.service');
const middleware = require('./src/middlewares/weather.middleware');

const weatherService = new WeatherService('https://www.dmi.dk');
const app = express()
const port = 3000

// Rate Limiting pr ip
app.use('/', expressRateLimit({
  windowMs: 60 * 60 * 1000, // 1 hour
  max: 10, // max 10 request per hour
  message: 'Too many request, try again later'
}))

// Returns the current weather from dmi.dk
app.get('/', middleware(weatherService), (req, res) => {
  return res.json({
    success: req.weatherData != null,
    data: req.weatherData,
    message: !req.weatherData ? 'Something went wrong. Try again' : undefined
  })
})

app.listen(port, () => console.log(`app listening on port ${port}!`))


